public abstract class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        if (x <= 0) {
            x = 0;
        }
        if (y <= 0) {
            y = 0;
        }

        this.x = x;
        this.y = y;
    }

    public Figure(){}

    public abstract double getPerimeter();

    public String toString()
    {
        return "(" + x + ", " + y + ")";
    }
}
