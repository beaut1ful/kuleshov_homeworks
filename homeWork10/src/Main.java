public class Main {

    public static void main(String[] args) {
        FigureMovement[] figures = new FigureMovement[]{
                new Circle(0, 0, 5),
                new Square(0, 0, 5),
                new Circle(10, 15, 7)
        };

        for (int i = 0; i < figures.length; i++) {
            System.out.println("Начальная позиция: " + figures[i].toString());
            figures[i].Move(1, 1);
            System.out.println("Конечная позиция: " + figures[i].toString());
            System.out.println();
        }
    }
}
