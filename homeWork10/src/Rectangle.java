public class Rectangle extends Figure {

    public int side1;
    public int side2;

    public Rectangle(int x, int y, int side1, int side2) {
        super(x, y);
        this.side1 = side1;
        this.side2 = side2;
    }

    public double getPerimeter() {
        return (side1 + side2) * 2;
    }
}
