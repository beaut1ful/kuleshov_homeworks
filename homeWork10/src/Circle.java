public class Circle extends Ellipse implements FigureMovement {
    public Circle(int x, int y, int radius1) {
        super(x, y, radius1, radius1);
    }

    @Override
    public double getPerimeter() {
        return 2 * radius1 * Math.PI;
    }

    @Override
    public void Move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
