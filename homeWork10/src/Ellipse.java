public class Ellipse extends Figure {

    protected int radius1;
    protected int radius2;
    public Ellipse(int x, int y, int radius1, int radius2) {
        super(x, y);
        if (radius1 > radius2){
            this.radius1 = radius1;
            this.radius2= radius2;
        }
        else {
            this.radius1 = radius2;
            this.radius2 = radius1;
        }
    }

    public double getPerimeter() {
        return 4 * ((Math.PI * radius1 * radius2 + (radius1 - radius2)) / (radius1 + radius2));
    }
}
