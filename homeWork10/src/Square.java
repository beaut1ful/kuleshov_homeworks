public class Square extends Rectangle implements FigureMovement{
    public Square(int x, int y, int side1) {
        super(x, y, side1, side1);
    }

    @Override
    public double getPerimeter() {
        return 4 * side1;
    }

    @Override
    public void Move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
