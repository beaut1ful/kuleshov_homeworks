public class Main {

    public static int indexFinder (int [] arr, int number) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number)
                return i;
        }
        return -1;
    }

    public static void valuesToLeft (int [] arr) {
        int[] arr2 = new int[arr.length];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0)
                arr2[j++] = arr[i];
        }
        for (int i : arr2) {
            System.out.print(i + " ");
        }
    }


    public static void main(String[] args) {
        int[] arr = {3, 2, 6, 5, 8};
        System.out.println(indexFinder(arr, 2));

        valuesToLeft(new int[] {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20});
    }
}
