import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int minDigit = 10;
        int a = scanner.nextInt();
        while (a != -1)
        {
            while (a != 0)
            {
                int lastDigit = a % 10;
                if (lastDigit < minDigit)
                    minDigit = lastDigit;
                a = a / 10;
            }
            a = scanner.nextInt();
        }
        System.out.println("Result = " + minDigit);
        System.out.println("Finish!");
    }
}
